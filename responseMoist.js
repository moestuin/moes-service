let sendMessageFlag = true; // TODO: Make this a timer based flag (send each day)
let moistLevel = 0;

const NEEDS_WATER_THRESHOLD = 750;

/**
 * Update the moistLevel value
 * @param message
 */
function updateMoistLevel (message) {
    moistLevel = parseInt(message);
}

/**
 * Send alert to the client "plants need water!"
 * @returns {string}
 */
function getAlertMoistLevel() {
    if (moistLevel > NEEDS_WATER_THRESHOLD) {
        if (sendMessageFlag) {
            sendMessageFlag = false;
            return `Hi, this is Moes. Time to water the garden! ${getSensorSentence()}`;
        }
    } else {
        sendMessageFlag = true;
    }
}

/**
 * Return the latest state of the Moist Sensor
 * @returns {string}
 */
function getResponseMoistLevel() {
    if (moistLevel > NEEDS_WATER_THRESHOLD) {
        return `Time to water the garden! ${getSensorSentence()}`;
    } else {
        return `You garden is happy. ${getSensorSentence()}`;
    }
}

/**
 * Standard ending of sentence with sensor value
 * @returns {string}
 */
function getSensorSentence() {
    return `Moist sensor value is ${moistLevel}`;
}

module.exports = {
    updateMoistLevel,
    getAlertMoistLevel,
    getResponseMoistLevel
};